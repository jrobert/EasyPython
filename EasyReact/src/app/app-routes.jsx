let React= require("react");
let Router = require('react-router');
let Route = Router.Route;
let Redirect = Router.Redirect;
let DefaultRoute = Router.DefaultRoute;
let Principal = require('./components/principal.jsx');
let ListeExercices = require('./components/ListeExercices.jsx');
let ListeGestionExercices = require('./components/ListeGestionExercices.jsx');

let EditeExercice = require('./components/EditeExercice.jsx');
let CreerExercice = require('./components/CreerExercice.jsx');
let Progressions = require('./components/Progressions.jsx');
let Progression = require('./components/Progression.jsx');
let EditeProgression = require('./components/EditeProgression.jsx');
let FaireExercice = require('./components/FaireExercice.jsx');
let Profil = require('./components/Profil.jsx');
let ExerciceDansProgression = require('./components/ExerciceDansProgression.jsx');

let VisualiserTentatives = require('./components/VisualiserTentatives.jsx');

let Login=require('./components/Login.jsx')
/** Routes: https://github.com/rackt/react-router/blob/master/docs/api/components/Route.md
  *
  * Routes are used to declare your view hierarchy.
  *
  * Say you go to http://material-ui.com/#/components/paper
  * The react router will search for a route named 'paper' and will recursively render its
  * handler and its parent handler like so: Paper > Components > Master
  */

let AppRoutes = (
  <Route name="root" path="/" handler={Principal}>
        <Route name="liste-exercices" handler={ListeGestionExercices}/>
        <Route name="exercices-resolus" handler={ListeExercices}/>

        <Route name="progressions" handler={Progressions}/>
        <Route name="exercice" path="exercice/:id" handler={FaireExercice}/>
        <Route name="prog" handler={ExerciceDansProgression}>
            <Route name="exerciceProgression" path="prog/:progression/:id" handler={FaireExercice}/>
        </Route>
        <Route name="gerer_exercice" path="gerer_exercice/:id" handler={EditeExercice}/>
        <Route name="creer_exercice" path="creer_exercice/" handler={CreerExercice}/>
        <Route name="progression" path="progression/:id" handler={Progression}/>
        <Route name="edite_progression" path="edite_progression/:id" handler={EditeProgression}/>
        <Route name="resultats" path="resultats/:id" handler={VisualiserTentatives}/>

        <Route name="profil"  handler={Profil}/>


   <DefaultRoute handler={Progressions}/>
  </Route>

);

module.exports = AppRoutes;
