var $ = require('jquery');
var React = require('react');
var mui = require('material-ui');
var {ClearFix,Dialog,TextField} = mui;

var Login = React.createClass({
  getInitialState() {
    return {
      error: false
    };
  },
  getStyles: function() {
    return {
      textfield: {
        marginTop: 24
      }
    };
  },
  handleSubmit(event) {
  $.ajax({
            type: 'POST',
            url: 'connexionAjax',
            data: {user:this.state.login, pass:this.state.passwd},
            success:function(reponse, status){
                if(reponse.login){
                    var user={id:reponse.id, name:this.state.login, groups:reponse.groups}
                    localStorage.setItem("user", JSON.stringify(user))
                    this.refs.dial.dismiss()
                    this.props.onLogin()
                    }
                else{
                    this.setState({error:true})
                }
            }.bind(this),
          });
  },

  handleChangeLogin : function (event) {
    this.setState({
      login: event.target.value
    });
  },
    handleChangePasswd : function (event) {
    this.setState({
      passwd: event.target.value
    });
  },

  show: function(){ 
    this.refs.dial.show();
  },


  render: function(){
     var styles = this.getStyles();
     var actions = [
      { text: 'Annuler' },
      { text: 'Envoyer', onTouchTap: this.handleSubmit}
    ];
    var errorText="";
    if(this.state.error)
        errorText="Identifiant ou mot de passe invalide"
    return (
         <Dialog
          ref="dial"
          title="Login"
          actions={actions}
          modal={true}>
          <ClearFix>
            <TextField
                style={styles.textfield}
                hintText=""
                floatingLabelText="Login"
                errorText={errorText}
                onChange={this.handleChangeLogin}
            />
            <br/>
            <TextField
                hintText=""
                type="password"
                floatingLabelText="Mot de passe"
                onChange={this.handleChangePasswd}
                onEnterKeyDown={this.handleSubmit}
            />
            <br/>
          </ClearFix>
        </Dialog>
    );
  }
});
module.exports=Login;
