var React = require('react');
var mui = require('material-ui');
var {List,ListItem,TextField, RaisedButton} = mui;

var ZoneEdition=React.createClass({
  contextTypes: {

  },
  propTypes: {
           listeErreursVisibles:React.PropTypes.object,
           erreursInvisibles:React.PropTypes.boolean,
           erreurs:React.PropTypes.object,
        },

        getDefaultProps: function() {},
        componentWillMount : function() {},
        componentWillReceiveProps: function() {},
        componentWillUnmount : function() {},
        _parseData : function() {},
        _onSelect : function() {},

        render: function() {
        console.log("TOTO")



        var mauvais_res_visibles=[];
        for (var i in this.props.listeErreursVisibles) {
                var erreur=this.props.listeErreursVisibles[i];
                var type="Entrée visible"
                var entree=erreur[0]
                var sortie_attendue=erreur[1]
                var sortie_prog=erreur[2]
                mauvais_res_visibles.push(
                 <ListItem primaryText={type}
                 secondaryText={"Sur l'entrée"+entree+" votre programme a renvoyé "+sortie_prog+" alors qu'on attendait "+ sortie_attendue}
                 secondaryTextLines={2}
                disabled={true}
            />
                    );
         }


        if(this.props.erreursInvisibles)
            mauvais_res_visibles.push(
                 <ListItem primaryText={"Entrée(s) invisible(s)"}
                 secondaryText={"Votre programme n'a pas renvoyé le bon résultat pour au moins une entrée cachée"}
                disabled={true} />

                )

        if(mauvais_res_visibles.length>0)
            mauvais_res_visibles=(
            <List subheader="Erreurs">
            <ListItem primaryText="Mauvais résultats">
                {mauvais_res_visibles}
            </ListItem>
            </List>
            )



        var erreurs={}
        if(this.props.erreurs.erreur){
        console.log("ERREURS"+this.props.erreurs)
        erreurs=
            <div>
                {this.props.erreurs.erreur}
                <pre>{this.props.erreurs.exception}</pre>
            </div>
        }

       return (
                <div style={this.props.style}>
                   {erreurs}
                   {mauvais_res_visibles}
               </div>
                );
         },
});

module.exports=Erreurs
