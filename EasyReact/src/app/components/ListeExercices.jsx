var $ = require('jquery');
let React = require('react');
var mui = require('material-ui');
var Exercice = require("./Exercice.jsx")
//var EditeExercice = require("./EditeExercice.jsx")
//var FaireExercice = require("./FaireExercice.jsx")
var summarize = require('summarize-markdown');

var Auth=require('../policies/Auth.jsx')
var Router = require('react-router');
var Link=Router.Link;

var {List,ListItem,TextField, RaisedButton,IconButton,FloatingActionButton,Paper} = mui;


var ListeExercices=React.createClass({
  contextTypes: {
    router: React.PropTypes.func,
    notification:React.PropTypes.func.isRequired,
    muiTheme: React.PropTypes.object,

  },


  componentDidMount: function() {
    $.get("api/v1/exerciceReussi/?format=json", function(result) {
      if (this.isMounted()) {
         this.setState({
           exercices: result["objects"],
            });
         }
      }.bind(this));
    },

  getInitialState: function() {
        return {
            exercices:[],
            recherche:"",
            };
        },

  getDefaultProps: function() {},
        componentWillMount : function() {},
        componentWillReceiveProps: function() {},
        componentWillUnmount : function() {},
        _parseData : function() {},
        _onSelect : function() {},



        handleChangeRecherche: function (event) {
            this.setState({
                recherche: event.target.value
            })
        },
  allerA: function(id){
 	return () => this.context.router.transitionTo('resultats', {id:id})

  },

  ajoutExercice: function(exercice)  {
    var exos=this.state.exercices;
    exos.push(exercice);
    this.setState({exercices:exos})
  },
  render: function() {
            var exercicesListe = this.state.exercices;
            var exercices=[];
            var recherche=this.state.recherche.toLowerCase();
            for (var i in exercicesListe) {
                var exercice=exercicesListe[i];
                var titre=exercice.titre
                var enonce=exercice.enonce
                var commentaires=exercice.commentaires
                var id=exercice.id
                if(titre.toLowerCase().indexOf(recherche)>-1 ||enonce.toLowerCase().indexOf(recherche)>-1||
                     commentaires.toLowerCase().indexOf(recherche)>-1 ){
                    exercices.push(
                    <ListItem
                        primaryText={titre}
                        secondaryText={summarize(enonce)}
                        secondaryTextLines={2}
                        onClick={this.allerA(id)}
                     />
                    );
                }
            }
            return (
                <div style={this.props.style}>
                    Les exercices que vous avez résolus.
                    <br/>
                   <TextField
                        hintText="Filtrer"
                        label='Filtrer dans le titre ou le sujet :'
                        onChange={this.handleChangeRecherche} />
                   <Paper zDepth="1">
                   <List>
                        {exercices}
                   </List>
                </Paper>
               </div>

                );
  },
});

module.exports=ListeExercices
