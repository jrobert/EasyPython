var React = require('react');
var mui = require('material-ui');
var CodeMirrorEditor = require("./CodeMirrorEditor.jsx")

var $ = require('jquery');
var {FlatButton,FloatingActionButton,List,ListItem,Snackbar,Card,CardText,CardHeader,Dialog}=require('material-ui')
var CodeMirrorE = React.createFactory(CodeMirrorEditor);




var VisualiserTentatives=React.createClass({
      contextTypes: {
        router: React.PropTypes.func,
        },

        propTypes: {
            id: React.PropTypes.number,

        },

       componentDidMount: function() {
        $.get("api/v1/tentative/?exercice="+this.props.params.id+"&format=json", function(result) {
        this.setState({resultats:result.objects})
       }.bind(this));
        },
        getInitialState: function() {
        return {resultats:[]};
            },
        getDefaultProps: function() {},

        componentWillMount : function() {},
        componentWillReceiveProps: function(newProps) {
       },
        componentWillUnmount : function() {},

        _parseData : function() {},
        _onSelect : function() {},


        render: function () {
            var res=[]
            for(var i in this.state.resultats)
                {
                    res.push(
                        CodeMirrorE({
                            style: {
                            border: '1px solid black',
                            margin:'1em 0'
                            },
                            textAreaClassName: ['form-control'],
                            textAreaStyle: {minHeight: '10em'},
                            mode: 'python',
                            theme: 'solarized',
                            lineNumbers: true,
                            value:this.state.resultats[i].contenu,
                        })
                    )
                }
                return (
                            <div style={this.props.style}>
                            {res}
                            </div>
                            )
                }
    })

module.exports=VisualiserTentatives
