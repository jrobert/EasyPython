var $ = require('jquery');
var React = require('react');
var mui = require('material-ui');
var CodeMirrorEditor = require("./CodeMirrorEditor.jsx")
var EditeMarkdown = require("./EditeMarkdown.jsx")

var {FlatButton, FloatingActionButton, IconButton,TextField}=mui
var CodeMirrorE = React.createFactory(CodeMirrorEditor);
var CreerExercice=React.createClass({
      contextTypes: {
        router: React.PropTypes.func
        },
        propTypes: {
            uri: React.PropTypes.string,
        },
      componentDidMount: function() {
                 this.setState({
                        titre: "",
                        src:"entrees_visibles=[\n        (1),\n        (12),\n        (4),\n]\nentrees_invisibles=[\n        (-4),\n        (5),\n        (-2),\n]\n\n@solution\ndef f(x):\n   return None"
                        });
                    this.refs.enonce.setState({value:""})
        },
      getInitialState: function() {
         return {
          error:false,
          msgsError: [],
          exo:{"moduleEns":""},
           src:""};
            },
        getDefaultProps: function() {},

        componentWillMount : function() {},
        componentWillReceiveProps: function() {},
        componentWillUnmount : function() {},

        _parseData : function() {},
        _onSelect : function() {},


      sauvegarder:  function(){
            var exo=this.state.exo
            exo["moduleEns"]=this.state.src
            exo["enonce"]=this.refs.enonce.state.value
            exo["commentaires"]=this.refs.commentaires.getValue()
            $.ajax({
                type: 'POST',
                url: 'api/v1/gestion_exercice/',
                data: JSON.stringify(exo),
                success:function(jqXHR, status)
                    {
                        this.context.router.transitionTo('liste-exercices')
                    }.bind(this),
                error:(function(jqXHR, status, erreur)
                    {
                        alert(jqXHR.responseJSON.exercice.erreurs)
                        this.setState(
                        {error:true, msgsError:jqXHR.responseJSON.exercice.erreurs}
                        );
                    }).bind(this),
            processData:  false,
            contentType: "application/json"
                });
            },

      render: function ()
            {


            return (
            <div style={this.props.style}>
            <TextField
                hintText="Titre"
                ref="titre"
                floatingLabelText="Titre de l'exercice"
                value={this.state.exo.titre}
                 onChange={function (e) {
                  var  exo=this.state.exo
                    exo.titre=e.target.value
                    this.setState({exo: exo});
                    }.bind(this)}
            />
            <EditeMarkdown ref="enonce"/>



            {CodeMirrorE({
                style: {border: '1px solid black'},
                textAreaClassName: ['form-control'],
                textAreaStyle: {minHeight: '10em'},
                value: this.state.src,
                mode: 'python',
                theme: 'solarized',
                lineNumbers: true,
                onChange: function (e) {
                    this.setState({src: e.target.value});
                    }.bind(this)
            })}

            <FlatButton
             className="pull-right"
             bsSize='small'
             onClick={this.sauvegarder}>
                Sauvegarder
            </FlatButton>

            <FlatButton
             className="pull-left" bsSize='small'
             onClick={() => this.context.router.transitionTo('liste-exercices')}
             >
                Retour
             </FlatButton>

             <TextField ref="commentaires"
                            floatingLabelText="Commentaires"
                            hintText="Commentaires"
                            multiLine={true}
                            fullWidth={true}
                            style={{
                                flex:'1',
                                minWidth:'240'
                            }}
            />

            </div>

          )
        },
    })

module.exports=CreerExercice
