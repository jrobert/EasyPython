var React = require('react');
var mui = require('material-ui');
var CodeMirrorEditor = require("./CodeMirrorEditor.jsx")
var Erreurs = require("./Erreurs.jsx")

var marked = require('marked');
var $ = require('jquery');
var {FlatButton,FloatingActionButton,List,ListItem,Snackbar,Card,CardText,CardHeader,Dialog}=require('material-ui')
var CodeMirrorE = React.createFactory(CodeMirrorEditor);



var Exemples=React.createClass({
propTypes: {
           exemples:React.PropTypes.object,
        },
render(){
    var exemples=[]
    for(var i in this.props.exemples)
    {
        var entree=this.props.exemples[i][0]
        var sortie=this.props.exemples[i][1]

        sortie=(typeof sortie !== 'undefined' && sortie!=null )?sortie.toString():"None"
        exemples.push(
            <li> Sur l'entrée <em>{entree.toString()}</em> votre fonction doit renvoyer <em>{sortie}</em> </li>
        )

    }
    return(
    <div> Exemples :
        <ul>
            {exemples}
        </ul>
     </div>
    )
}
});



var FaireExercice=React.createClass({
      contextTypes: {
        router: React.PropTypes.func,
        },

        propTypes: {
            uri: React.PropTypes.string,
            id: React.PropTypes.number,

        },

       componentDidMount: function() {
        $.get("api/v1/exercice/"+this.props.params.id+"/?format=json", function(result) {
        this.props.changeSousTitre(result.titre)
        if (this.isMounted()) {
        this.setState({
           ennonce:result.enonce,
           titre:result.titre,
           exemples:result.exemples
         });
         this.refs.codemirror.editor.setValue("def " + result.moduleEns.nom_solution+"("+result.moduleEns.arguments.join()+"):\n    return None")
       }
       }.bind(this));
        },
        getInitialState: function() {
         return {
           src:"",
           ennonce:"",
           exemples:[],

           };
            },
        getDefaultProps: function() {},

        componentWillMount : function() {},
        componentWillReceiveProps: function(newProps) {
        if(newProps.params.id!=this.props.params.id)
        {
        $.get("api/v1/exercice/"+newProps.params.id+"/?format=json", function(result) {
        if(result.titre != this.state.titre)
          this.props.changeSousTitre(result.titre)

        if (this.isMounted()) {
        this.setState({
           ennonce:result.enonce,
           titre:result.titre,
           exemples:result.exemples

         });
         this.refs.codemirror.editor.setValue("def " + result.moduleEns.nom_solution+"("+result.moduleEns.arguments.join()+"):\n    return None")
       }
       }.bind(this))
       };},
        componentWillUnmount : function() {},

        _parseData : function() {},
        _onSelect : function() {},


        envoyer:  function(){
            $.ajax({
            type: 'POST',
            url: 'api/v1/tentative/',
            data: JSON.stringify({"contenu":this.refs.codemirror.editor.getValue(),"titre":this.state.titre}),
            success:function(jqXHR, status){
                this.refs.customDialog.show();

            }.bind(this),
            error:(function(jqXHR, status, erreur)
            {
                this.refs.snackbar.show()
                if(jqXHR.responseJSON.tentative._invalide!=null)
                {
                this.refs.erreurs.setState({
                    erreurs:jqXHR.responseJSON.tentative._invalide,
                    listeErreursVisibles:[],
                    erreursInvisibles:false
                    })
                }
                else
                if(jqXHR.responseJSON.tentative._mauvais_resultats!=null)
                {
                    this.refs.erreurs.setState({
                    erreurs:"",
                    listeErreursVisibles:jqXHR.responseJSON.tentative._mauvais_resultats.visibles,
                    erreursInvisibles:jqXHR.responseJSON.tentative._mauvais_resultats.invisibles!=null}
                    )
                }
                else
                if(jqXHR.responseJSON.tentative.erreur!=null)
                { /* TODO a faire mieux: traitement du cas de depassement cpu */
                    this.refs.erreurs.setState({
                    erreurs:{erreur:true,exception:jqXHR.responseJSON.tentative.erreur},
                    listeErreursVisibles:[],
                    erreursInvisibles:false}
                    )
                }


            }).bind(this),
            processData:  false,
            contentType: "application/json"
            });
            },

        render: function () {
            var customActions = [
                <FlatButton
                key={1}
                label="Je suis fier !"
                secondary={true}
                onTouchTap={()=>{
                    var path=this.context.router.getCurrentParams()
                    if(path.progression && path.progression!=0)
                          this.context.router.transitionTo('progression', {id:path.progression})
                    else
                          this.context.router.transitionTo('liste-exercices')
                }.bind(this)} />,
                <FlatButton
                key={2}
                label="C'était trop facile."
                primary={true}
                onTouchTap={()=>{
                    var path=this.context.router.getCurrentParams()
                    if(path.progression && path.progression!=0)
                          this.context.router.transitionTo('progression', {id:path.progression})
                    else
                          this.context.router.transitionTo('liste-exercices')
                }.bind(this)}
                />
             ];

        return (
    	<div style={this.props.style}>
            <Dialog
                ref="customDialog"
                title="Félicitations !"
                actions={customActions}
                modal={this.state.modal}>
                Vous avez brillament résolu l'exercice.
            </Dialog>
        <div
        id="ContenuPrincipal"
        style={{
        display:'flex',
        flexWrap: 'wrap',
        }}>

            <div
              id="Enonce"
              style={{
              flex: '1 100%',
              padding: '20px 20px 0 20px',
            }}>
                 <Card >
                    <CardText>
                        <div dangerouslySetInnerHTML={{__html: marked(this.state.ennonce, {sanitize: true})}} />
                        <Exemples exemples={this.state.exemples}/>
                    </CardText>
                 </Card>
            </div>

        <div
        id="CodeEtBouton"
        style={{
                minWidth: '60%',
                flex: '2',
                position:'relative',
                padding:'20px 20px 0 20px',
                }}
        >
            {CodeMirrorE({
                style: {
                border: '1px solid black',
                },
                textAreaClassName: ['form-control'],
                textAreaStyle: {minHeight: '10em'},
                ref:'codemirror',
                mode: 'python',
                theme: 'solarized',
                lineNumbers: true,
            })}

            <FloatingActionButton onClick={this.envoyer}
                style={{
                position: 'absolute',
                bottom: '-70px',
                right: '0px',
            }}>
                Envoyer
            </FloatingActionButton>
        </div>

        <div
            id="retours"
            style=
             {{
                flex: '1',
                padding: '20px 20px 0 20px'
             }}
        >

            <Card
                style= {{
                    minWidth: '320px',
                    minHeight:'100%',
                }}
            >
                <CardText>

                    <Erreurs ref='erreurs'
                    style={{
                    maxWidth: 'inherit'
                    }}
                />
                </CardText>
            </Card>
        </div>

        <Snackbar
          ref="snackbar"
          message="Votre code ne fait pas ce qui est attendu."
          action="Ok"
          autoHideDuration={10000}
          openOnMount={false}
          onActionTouchTap={function(){this.refs.snackbar.dismiss()}.bind(this)}
        />
      </div>
    </div>
       )
        },
    })

module.exports=FaireExercice
