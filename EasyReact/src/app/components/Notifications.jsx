var React=require("react");
var mui = require('material-ui');

var {ClearFix,Dialog,IconButton} = mui;

var Notifications = React.createClass({
    contextTypes: {
                      muiTheme: React.PropTypes.object,
                  },

    getInitialState() {
        return {
            messages:[]
        };
    },
    getStyles: function() {
                   return {

                       textfield: {
                                      marginTop: 24
                                  }
                   };
               },
    componentDidUpdate: function(){

                        },

    supprimerNotification:function(numero){
                              var msgs=this.state.messages;
                              msgs.splice(numero,1)
                                  this.setState({messages:msgs})

                          },

    show: function(chaine){
              var msgs=this.state.messages
                  msgs.push(chaine)
                  this.setState({messages:msgs})
                  setTimeout(() => { this.supprimerNotification(0); }, 5000)
                  /* TODO Attention : le timer ne fait pas exactement ce qu'on aimerait. Le problème survient lorsqu'on a fermé la
                     notification manuellement
                   */
          },


    getSpacing() {
        return this.context.muiTheme.rawTheme.spacing;
    },

    render:function(){

               var res=[]
                   for(var msg in this.state.messages)
                   {
                       res.push(
                               <div ref={msg}
                               style={{
                                   float:"left",
                       clear:"left",
                       minWidth: "288",
                       maxWidth: "568",
                       backgroundColor: "Tomato",}}
                       >
                       {this.state.messages[msg]}
                       <IconButton
                       onClick={function(){
                           this.supprimerNotification(msg)}.bind(this)
                       }
                       style={{
                           float:"right"}}
                           >
                           <i className="material-icons" >clear</i>
                           </IconButton>
                           </div>
                           )

                   }

               return (
                       <div
                       style={{
                           position: "fixed",
                      left: "0",
                      borderRadius: "2",
                      padding: '0px ' + this.context.muiTheme.rawTheme.spacing.desktopGutter + 'px',
                      height: this.context.muiTheme.rawTheme.spacing.desktopSubheaderHeight,
                      lineHeight: this.context.muiTheme.rawTheme.spacing.desktopSubheaderHeight + 'px',
                      zIndex: 10,
                      bottom: this.context.muiTheme.rawTheme.spacing.desktopGutter,
                      marginLeft: this.context.muiTheme.rawTheme.spacing.desktopGutter,
                       }}
                       >
                       {res}
                       </div>
                      )
           }

})

module.exports=Notifications;
