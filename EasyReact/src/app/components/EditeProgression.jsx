var $ = require('jquery');
var EditeMarkdown = require("./EditeMarkdown.jsx");
//require("./EditeExercice.jsx")
//require("./FaireExercice.jsx")
var React = require('react');
var mui = require('material-ui');
var Auth=require('../policies/Auth.jsx')
var Router = require('react-router');
var marked = require('marked');
var sortable = require('react-sortable-mixin');
var summarize = require('summarize-markdown');
var {Paper, List, FloatingActionButton, IconButton, TextField, ListItem} = mui;


var Item = React.createClass(
{
  mixins: [sortable.ItemMixin],
  render: function()
   {
                 var bouton=<IconButton
                                tooltip="Supprimer"
                                onClick={() => this.props.onClick(this.props.exo)}
                             >
                                <i className="material-icons" >clear</i>
                             </IconButton>
                return <div>
                            <b> {this.props.exo.titre} </b>
                            {summarize(this.props.exo.enonce)}
                            {bouton}
                       </div>
   }
});

var ListeExosTriable=React.createClass({
  mixins: [sortable.ListMixin],
  componentDidMount: function() {
    // Set items' data, key name `items` required
        this.setState({ items: this.props.items });
  },
  ajouterExercice:function(exo){
        var items=this.state.items
        items.push(exo)
        this.setState({items:items})
  },
  retirerExercice:function(id){
        var items=this.state.items
        var index=-1
        for(var i in items)
            if(items[i].id==id)
                index=i
        if(index>-1)
            items.splice(index,1)
        this.setState({items:items})
  },
  setExercices:function(exercices){
  this.setState({items:exercices})
  },

  render: function() {
    var items = this.state.items.map(function(exo, i) {
      // Required props in Item (key/index/movableProps)
      return <Item onClick={this.props.onClick} key={exo.titre} id={exo.id} exo={exo} index={i} {...this.movableProps}/>;
    }, this);
    return <ul
            style={this.props.style}>
                {items}
           </ul>;
  }
});


var ExoAjoutable=React.createClass(
{

render :function()
    {
            var bouton=<IconButton
                            tooltip="Ajouter"
                            onClick={()=>this.props.onClick(this.props.exo)}
                       >
                            <i className="material-icons" >add</i>
                       </IconButton>
            return <div>
                            <b> {this.props.exo.titre} </b>
                            {summarize(this.props.exo.enonce)}
                            {bouton}
                       </div>
    }
})


var ListeExosAjoutables=React.createClass({
    getInitialState: function()
        {
        return {
            exercices:this.props.exercices,
            recherche:"",
            exosEnleves:[],
            };
        },

    enleverExo:function(id)
    {
        var index=-1
        var exos=this.state.exercices
        for(var i in exos)
            if(exos[i].id==id)
                index=i
        if(index>-1)
            exos.splice(index,1)
        this.setState({exercices:exos})
    },
    ajouterExo:function(exo)
    {
        var exercices=this.state.exercices
        exercices.push(exo)
        this.setState({exercices:exercices})
    },
    setExercices:function(exercices){
    this.setState({exercices:exercices})
    },
    handleChangeRecherche: function (event)
    {
        this.setState({
                recherche: event.target.value
        })
    },
    listeExos:function()
    {
    var exercicesListe = this.state.exercices;
    var exercices=[];
    var recherche=this.state.recherche.toLowerCase();
    for(var i in exercicesListe )
        {
         var exercice=exercicesListe[i];
         var titre=exercice.titre
         var enonce=exercice.enonce
         var commentaires=exercice.commentaires

         var id=exercice.id
         if(this.state.exosEnleves.indexOf(id)==-1 &&
                (titre.toLowerCase().indexOf(recherche)>-1 ||enonce.toLowerCase().indexOf(recherche)>-1 || commentaires.toLowerCase().indexOf(recherche)>-1)
           ){
            exercices.push(exercicesListe[i])
            }
         }
         return exercices
    },

    render: function()
    {
            var exos=this.listeExos()
            var exercices=[]
            for (var i in exos)
                {
                    exercices.push(
                        <ExoAjoutable key={i} exo={exos[i]} onClick={(exo)=>{ this.props.onClick(exo); this.enleverExo(exo.id)}}/>
                    );
                }

            return (
            <div>
            <TextField
                        hintText="Filtrer"
                        label='Filtrer dans le titre ou le sujet :'
                        onChange={this.handleChangeRecherche} />
                   <List>
                        {exercices}
                   </List>
            </div>
                );
    },
})





function  calculerComplement(tousExos,exos){
     var exercices=exos.slice()
      var complement=tousExos.slice()
      for(var i in exercices) {
            var index=-1
            for(var j in complement){
                if(complement[j].id==exercices[i].id)
                    index=j
            }
            if(index>-1)
                complement.splice(index, 1)
        }
      return complement
  }


var EditeProgression=React.createClass({
  contextTypes: {
    router: React.PropTypes.func,
    muiTheme: React.PropTypes.object,
    notification:React.PropTypes.func.isRequired
  },

  propTypes: {
                        titre: React.PropTypes.string,
                        sujet: React.PropTypes.string,
                        exercices: React.PropTypes.array,
                        tousExercices: React.PropTypes.array,
                        id: React.PropTypes.number,
  },

  getInitialState: function() {
        if(this.props.titre)

           return {
                    titre:this.props.titre,
                exercices:this.props.exercices,
                sujet:this.props.sujet,
                tousExercices:this.props.tousExercices
               };
        else
            return{
                titre:"",
                exercices:[],
                tousExercices:[],
                sujet:"",

            }
  },

  getDefaultProps: function() {},
  componentWillMount : function() {},
  componentWillReceiveProps: function() {},
  componentWillUnmount : function() {},

 componentDidMount: function() {
        $.get("api/v1/progression/"+this.props.params.id+"/?format=json",
            function(result)
                {
                    if (this.isMounted()) {
                        this.setState({
                        exercices: result.listeExercices,
                        });
                        this.refs.exercices.setExercices(result.listeExercices)

                        this.refs.sujet.setState({value:result.sujet})
                        this.refs.titre.setValue(result.titre)
                    }
                    $.get("api/v1/exercice/?format=json", function(res) {
                        if (this.isMounted()) {
                            this.refs.listeAjoutables.setExercices(calculerComplement(res["objects"], result.listeExercices ))
                    }}.bind(this))
            }.bind(this));


        },



  _parseData : function() {},
  _onSelect : function() {},
  sauvegarder : function(){

            var exos=[]
            for(var i in this.refs.exercices.state.items)
                {
                    var e=this.refs.exercices.state.items[i]
                    exos.push(e.id)
                }
            var progression={listeExercices:"["+exos.toString()+"]",
                        titre:this.refs.titre.getValue(),
                        sujet:this.refs.sujet.state.value,
                        id:this.props.params.id}

            $.ajax({
                type: 'POST',
                url: 'api/v1/progression/',
                data: JSON.stringify(progression),
                success:function(jqXHR, status)
                    {
                        this.context.notification("Progression sauvegardée")
                    }.bind(this),
                error:function(jqXHR, status, erreur)
                    {
                       this.context.notification("Erreur de sauvegarde")
                    }.bind(this),
                processData:  false,
                contentType: "application/json",
                    })
            },

  ajouterExo:function(exo){
        this.refs.exercices.ajouterExercice(
        exo
        )
  },

  retirerExo:function(exo){
        this.refs.exercices.retirerExercice(
            exo.id
        )
        this.refs.listeAjoutables.ajouterExo(exo)
  },


  render : function(){

        var exercices=this.state.exercices.slice()
        var complement=calculerComplement(this.state.tousExercices,exercices)

        return (
        <div style={this.props.style}>
            <TextField
                    style={{fontSize:"x-large"}}
                    hintText="Titre de la progression"
                    ref="titre"
                    defaultValue={this.state.titre}
            />

            <Paper>
                <EditeMarkdown value={this.state.sujet} ref="sujet"/>
            </Paper>

            <div id="Exercices"
               style={{
                    display:'flex',
                    flexWrap: 'wrap',
               }}>
              <div
                  style={{
                            flex: '1',
                            position:'relative',
                            padding:'20px 20px 0 20px',
                            width:"50%",
                            boxSizing:'border-box'
                  }}>
                        < ListeExosTriable
                                ref="exercices"
                                items={exercices}
                                onClick={(exo)=>this.retirerExo(exo)}
                        />
              </div>

              <div
                style={{
                    flex: '1',
                    padding: '20px 20px 0 20px',
                    width:"50%",
                    boxSizing:'border-box'
                    }} >
                        <ListeExosAjoutables ref="listeAjoutables" exercices={complement} onClick={(exo)=>this.ajouterExo(exo)}/>
              </div>
            </div>

            <FloatingActionButton onClick={this.sauvegarder}
                            style={{
                            float:"right",
                            margin:this.context.muiTheme.rawTheme.spacing.desktopGutter+'px',
            }}>
                            Enregistrer
            </FloatingActionButton>

        </div>

        )
        }
  })


module.exports = EditeProgression;
