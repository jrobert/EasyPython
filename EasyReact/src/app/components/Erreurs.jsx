var React = require('react');
var mui = require('material-ui');
var {List,ListItem,TextField, RaisedButton} = mui;

var Erreurs=React.createClass({
  contextTypes: {

  },
  getInitialState: function() {
            return{
              listeErreursVisibles:[],
           erreursInvisibles:false,
           erreurs:{},
           }
        },

        getDefaultProps: function() {},
        componentWillMount : function() {},
        componentWillReceiveProps: function() {},
        componentWillUnmount : function() {},
        _parseData : function() {},
        _onSelect : function() {},

        render: function() {
        var mauvais_res_visibles=[];
        for (var i in this.state.listeErreursVisibles) {
                var erreur=this.state.listeErreursVisibles[i];
                var type="Entrée visible"
                var entree=erreur[0]
                var sortie_prog=erreur[1]
                var sortie_attendue=erreur[2]
                mauvais_res_visibles.push(


                 <ListItem primaryText={type}
                 secondaryText={"Sur l'entrée"+entree+" votre programme a renvoyé "+sortie_prog+" alors qu'on attendait "+ sortie_attendue}
                 secondaryTextLines={3}
                disabled={true}
            />
                    );
         }

        if(this.state.erreursInvisibles)
            mauvais_res_visibles.push(
                 <ListItem primaryText={"Entrée(s) invisible(s)"}
                 secondaryText={"Votre programme n'a pas renvoyé le bon résultat pour au moins une entrée cachée"}
                disabled={true}
                secondaryTextLines={2}
        />
                )

        var err=false
        if(mauvais_res_visibles.length>0)
        {
            err=true
            mauvais_res_visibles=(
            <List subheader="Erreurs">

                {mauvais_res_visibles}
            </List>
            )
        }


        var erreurs={}
        if(this.state.erreurs.erreur && (this.state.erreurs.erreur!=null)){
        err=true
        erreurs=
            <div>
                    <h1> Erreur d'exécution </h1>
                    <pre>{this.state.erreurs.exception}</pre>
            </div>
        }


        if(err)
           return (
                <div style={this.props.style}>
                   {erreurs}
                   {mauvais_res_visibles}
               </div>
                );

        else
            return (
                <div> Bonne chance ! </div>
            )
         },
});

module.exports=Erreurs
