var $ = require('jquery');
var React = require('react');
var Router = require('react-router');
var mui = require('material-ui');
var { MenuItem, LeftNav, Styles } = mui;
var Auth=require('../policies/Auth.jsx')

var { Colors, Spacing, Typography } = Styles;
var defaultMenuItems=[
    { route: 'progressions', text: 'Les sujets' },
    { route: 'exercices-resolus', text: 'Vos exercices résolus' },
    { route: 'profil', text: 'Profil' },

  ];


var menuItemsProfs=[
    { route: 'progressions', text: 'Les sujets' },
    { route: 'liste-exercices', text: 'Les exercices' },
    { route: 'exercices-resolus', text: 'Vos exercices résolus' },
    { route: 'profil', text: 'Profil' },

  ];


/*var menuItems = defaultMenuItems.slice() */
var menuItems = defaultMenuItems

if(Auth.permit("enseignant"))
        {
        menuItems=menuItemsProfs;
        }
        else
        {
        menuItems=defaultMenuItems
        }

class AppLeftNav extends React.Component {

    constructor() {
        super();
        this.toggle = this.toggle.bind(this);
        this._getSelectedIndex = this._getSelectedIndex.bind(this);
        this._onLeftNavChange = this._onLeftNavChange.bind(this);

        this._onHeaderClick = this._onHeaderClick.bind(this);
    }

    getStyles() {
        return {
            cursor: 'pointer',
                //.mui-font-style-headline
                fontSize: '24px',
                color: Typography.textFullWhite,
                lineHeight: Spacing.desktopKeylineIncrement + 'px',
                fontWeight: Typography.fontWeightLight,
                backgroundColor: Colors.cyan500,
                paddingLeft: Spacing.desktopGutter,
                paddingRight: Spacing.desktopGutter,
                paddingTop: '0px',
                marginBottom: '8px'
        };
    }

    render() {

        var header = (
                <div style={this.getStyles()} onTouchTap={this._onHeaderClick}>
                EasyPython
                </div>
                );

        return (
                <LeftNav
                ref="leftNav"
                docked={false}
                isInitiallyOpen={false}
                header={header}
                menuItems={menuItems}
                selectedIndex={this._getSelectedIndex()}
                onChange={this._onLeftNavChange}
                />
               );
    }

    toggle() {

        if(Auth.permit("enseignant"))
        {
        menuItems=menuItemsProfs;
        }
        else
        {
        menuItems=defaultMenuItems
        }
      /*
        var path=this.context.router.getCurrentParams()

        if(path.progression &&
           path.progression != 0 &&
           (menuItems.length==3 || menuItems[4].params.progression!=path.progression) )
           {
            $.get("api/v1/progression/"+path.progression+"/?format=json", function(result) {
                menuItems.push( { type: MenuItem.Types.SUBHEADER, text: result.titre })
                for(var i in result.listeExercices){
                    menuItems.push( {
                        route:"exerciceProgression",
                        params: {"progression":path.progression,
                                  "id":result.listeExercices[i].id},
                        text: result.listeExercices[i].titre
                                    })
                }
            }.bind(this)
         )
         }
         if(!path.progression)
                menuItems.length=3
        */
        this.refs.leftNav.toggle();

       // this.refs.leftNav.toggle();
    }

    _getSelectedIndex() {
        var currentItem;
        for (var i = menuItems.length - 1; i >= 0; i--) {
            currentItem = menuItems[i];
            if (currentItem.route && this.context.router.isActive(currentItem.route)
                && (!currentItem.params || this.context.router.isActive(currentItem.route,currentItem.params)))
                return i;
        }
    }

    _onLeftNavChange(e, key, payload) {
        this.context.router.transitionTo(payload.route,payload.params);
    }


    _onHeaderClick() {
        this.context.router.transitionTo('root');
        this.refs.leftNav.close();
    }

}

AppLeftNav.contextTypes = {
    router: React.PropTypes.func
};

module.exports = AppLeftNav;
