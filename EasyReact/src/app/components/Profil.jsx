var $ = require('jquery');
var React = require('react');
var mui = require('material-ui');
var {ClearFix,Dialog,TextField} = mui;

var Profil = React.createClass({

  getInitialState() {
    return {
      error: false
    };
  },
  getStyles: function() {
    return {
      textfield: {
        marginTop: 24
      }
    };
  },
  componentDidMount: function() {

    $.ajax({
    type:'GET',
    url:'api/v1/auth/user/?format=json',
    success:function(reponse){
                if(reponse.objects){
                    var user=reponse.objects[0]
                    this.setState({user:user})
                    var user2={id:user.id, name:user.username, groups:user.groups}
                    localStorage.setItem("user", JSON.stringify(user2))
                    }
                else{
                    this.setState({error:true})
                }
            }.bind(this),
    error:function(reponse){
                    this.setState({error:true})
            }.bind(this),
    contentType: "application/json"
    }

      );
  },

  render: function(){

       if(this.state.error){
                return (<div> Il faut se logger </div>)
        }
       else
        if(this.state.user){
            return (

                <div style={this.props.style}>
                <h1> Profil (rien d'excitant ici ..)</h1>
                         <p>Utilisateur : {this.state.user.username}
                        </p>
                        <p>Dernier login : {this.state.user.last_login}
                        </p>
                        <p>Groupes : {this.state.user.groups}
                         </p>
                    </div>)
                }

       return (
       <div></div>
        )
    }

  }
  );
module.exports=Profil;
