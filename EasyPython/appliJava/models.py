from django.db import models
from django.contrib.auth.models import User

class ExerciceJava(models.Model):
    titre = models.CharField(max_length=200, unique=True)
    auteur = models.CharField(max_length=200)
    enonce = models.TextField()
    unitTest=models.textField()
    moduleEns = models.TextField()
    resultatsEns=models.TextField()
    commentaires = models.TextField()
    def __str__(self):
        return self.titre

    def get_date(dt):
        return dt.strftime("%Y-%M-%d")
