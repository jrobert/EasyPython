from django.conf.urls import patterns, url, include
from .api import ExerciceJavaResource
from tastypie.api import Api
#from .views import CreerAideView, testerSolution

v1_api = Api(api_name='v1')
exercice_resource=ExerciceJavaResource()

v1_api.register(exercice_resource)

urlpatterns = patterns(
                       url (r'^api/', include(v1_api.urls)),
)
