#!/usr/bin/python3 
from . import test_fonction
from .resultats import resultats
import json,sys,os,inspect
import time
from tempfile import NamedTemporaryFile
import subprocess

def test_unit(unitaire_ens, solution_ens):
    


class ExerciceJava:

    def __init__(self,module):
        self.messages=[]
        self.messagesErreur=[]
        self.messagesInfo=[]
        self.module_ens=None
        self.entrees_visibles=[]
        self.entrees_invisibles=[]
        self.solutions_visibles=[]
        self.solutions_invisibles=[]
        self.temps=None
        self.solution=None
        self.nom_solution=None
        self.arguments=None
        self.module_charge=False
        if (self.charger_module(module) and self.parser_module()):
            self.module_charge=True

    def tester_solution_etu(self,nom_module_etu):
        return ""
    def tester_solution_ens(self):
        try:
            time0=time.time()
            self.solutions_visibles=test_fonction.test_fonction(self.solution,self.entrees_visibles)
            self.solutions_invisibles=test_fonction.test_fonction(self.solution,self.entrees_invisibles)
            temps=time.time()-time0
            self.temps=temps
            return True
        except test_fonction.Erreur as e:
            self.error=True
            self.messagesErreur.append( str(e) )
            return False
    def afficher(self):
        attrs=["messages","messagesErreur","messagesInfo","entrees_visibles","entrees_invisibles","solutions_visibles","solutions_invisibles","temps","nom_solution","arguments"]
        res={attr:self.__dict__[attr] for attr in attrs if self.__dict__[attr] }
        res["solutions_visibles"]=[(str(list(x))[1:-1],str(y)) for (x,y) in self.solutions_visibles]
        print(json.dumps(res))
