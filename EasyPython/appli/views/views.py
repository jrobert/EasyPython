from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

#from appli.forms import *
from appli.models import *

from appli.models import Exercice

def exercices(request):
	titre = "Exercices"
	return render(request, 'base-react-material.html', locals())




#@login_required
#def aide(request):
#	obt = cultive(request.user)
#	titre = "Aide"
#	return render(request, 'appli/aide-react.html', locals())

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def connexionAjax(request):
	error = False
	if request.is_ajax():
		username=request.POST["user"]
		password = request.POST["pass"]
		user = authenticate(username=username, password=password)
		if user:
			login(request, user)
			return JsonResponse({"login":True,"id":user.id, "groups":[x.name for x in user.groups.all()]})
		else:
			return JsonResponse({"login":False})


def deconnexion(request):
    logout(request)
    return redirect(reverse(connexion))


@permission_required('exercice.can_delete')
def duplicate_exercice(request, exo_id):
    e=Exercice.objects.get(pk=exo_id)
    e.pk=None
    n=0
    while(Exercice.objects.filter(titre=e.titre+" (copie "+str(n) + ")")):
        n=n+1
    e.titre=e.titre+" (copie "+str(n) + ")"
    e.save()
    return JsonResponse({"duplicate":True, "titre":e.titre, "id":e.id})
