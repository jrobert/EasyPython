from django.core.management.base import BaseCommand
import yaml
from django.db import models, migrations
from appli.models import get_tag_par_nom

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Fait un dump des exercices dans le fichie passé en argument'
	
	def handle(self, *args, **options):
		fichier, *suite=args
		fichier=open(fichier,"+w")
	
		#import des modeles.
		from appli.models import Exercice
	
		# On crée les vins.
		i=0
		liste=[]
		for b in Exercice.objects.all():
			b=b.__dict__
			del b['_state']
			liste.append(b)
		fichier.write(yaml.dump(liste,  default_flow_style=False))

