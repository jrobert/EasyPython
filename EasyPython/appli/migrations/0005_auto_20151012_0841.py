# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0004_exercice_resultatsens'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exercice',
            name='tags',
        ),
        migrations.DeleteModel(
            name='Tag',
        ),
        migrations.AddField(
            model_name='exercice',
            name='commentaires',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
