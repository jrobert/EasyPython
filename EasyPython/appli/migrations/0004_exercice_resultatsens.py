# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0003_progression_titre'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercice',
            name='resultatsEns',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
