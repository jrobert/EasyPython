# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0007_creer_liste_exos'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='progression',
            name='listeExercices',
        ),
    ]
