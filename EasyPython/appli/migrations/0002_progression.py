# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Progression',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sujet', models.TextField()),
                ('listeExercices', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
