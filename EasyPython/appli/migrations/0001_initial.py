# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Aide',
            fields=[
                ('cle', models.CharField(unique=True, max_length=200, primary_key=True, serialize=False)),
                ('definition', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=200)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Date de parution')),
                ('lien', models.CharField(max_length=200)),
                ('affichage', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Badge',
            fields=[
                ('nom', models.CharField(max_length=200, primary_key=True, serialize=False)),
                ('image', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Etape',
            fields=[
                ('titre', models.CharField(max_length=200, primary_key=True, serialize=False)),
                ('nb', models.IntegerField(null=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Exercice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=200, unique=True)),
                ('auteur', models.CharField(max_length=200)),
                ('enonce', models.CharField(max_length=200)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Date de parution')),
                ('moduleEns', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lot',
            fields=[
                ('titre', models.CharField(max_length=200, primary_key=True, serialize=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrdreEtape',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num', models.IntegerField(unique=True)),
                ('etape', models.ForeignKey(to='appli.Etape')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('nom', models.CharField(max_length=200, primary_key=True, serialize=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tentative',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reussi', models.BooleanField(default=False)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Date de la tentative')),
                ('contenu', models.TextField()),
                ('titre', models.ForeignKey(to='appli.Exercice')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Utilisateur',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('groupe', models.IntegerField()),
                ('badges', models.ManyToManyField(to='appli.Badge')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='lot',
            name='etapes',
            field=models.ManyToManyField(to='appli.OrdreEtape'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='exercice',
            name='tags',
            field=models.ManyToManyField(to='appli.Tag'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='etape',
            name='exercices',
            field=models.ManyToManyField(to='appli.Exercice'),
            preserve_default=True,
        ),
    ]
