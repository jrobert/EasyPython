# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appli', '0002_progression'),
    ]

    operations = [
        migrations.AddField(
            model_name='progression',
            name='titre',
            field=models.CharField(default='TOTO', unique=True, max_length=200),
            preserve_default=False,
        ),
    ]
