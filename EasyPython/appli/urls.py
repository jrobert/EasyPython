from django.conf.urls import patterns, url, include
#from .api import ExerciceResource,AideResource,UserResource,GestionExerciceResource,ResumeExerciceResource,TentativeResource, ProgressionResource, ExerciceReussiResource
from .api import ExerciceResource,UserResource,GestionExerciceResource,ResumeExerciceResource,TentativeResource, ProgressionResource, ExerciceReussiResource
from tastypie.api import Api
#from .views import CreerAideView, testerSolution

v1_api = Api(api_name='v1')
exercice_resource=ExerciceResource()
exercice_reussi_resource=ExerciceReussiResource()
resume_exercice_resource=ResumeExerciceResource()
gestion_exercice_resource=GestionExerciceResource()
tentative_resource=TentativeResource()
progression_resource=ProgressionResource()

#aide_resource=AideResource()
user_resource=UserResource()

v1_api.register(exercice_resource)
v1_api.register(resume_exercice_resource)
v1_api.register(gestion_exercice_resource)

#v1_api.register(aide_resource)
v1_api.register(user_resource)
v1_api.register(tentative_resource)

v1_api.register(progression_resource)
v1_api.register(exercice_reussi_resource)


urlpatterns = patterns('appli.views',
                       #url(r'^test/$', 'test'),
                       #url(r'^accueil$', 'accueil',),
                       #url(r'^contact$', 'contact'),
                       #url(r'^recherche_exercice$', 'rechercher_exercice'),
                       #url(r'^recherche_parcours$', 'rechercher_parcours'),
                       #url(r'^profil$', 'profil'),
                       #url(r'^badges$', 'badges'),
                       #~ url(r'^profil/(?P<id>\d+)/(?P<categorie>\d+)/$', 'profil'),
                       #url(r'^profilP$','profilProf'),
                       #url(r'^creation_exercice$', 'creer_exercice'),
                       #url(r'^creation_parcours', 'creer_parcours'),
                       #url(r'^creation_aide$', CreerAideView.as_view(), name='creation_aide'),
                       #url(r'^exercice/(?P<id_exo>\d+)/$','exercice'),
                       #url(r'^aide$','aide'),
                       #url(r'^test_exos/(?P<exercice>\d+)/$','testerExercice'),
                       #url(r'^connexion$','connexion'),
                       
                      url(r'^connexionAjax$','connexionAjax'),
		      url(r'^api/dupliquer/(?P<exo_id>\d+)$','duplicate_exercice'),
                       url(r'^deconnexion$','deconnexion'),
                       url (r'^api/', include(v1_api.urls)),
                       url(r'^$','exercices'),

                       #url(r'^toto','testerSolution'),

)
