#!/usr/bin/python3 
from . import test_fonction
from .resultats import resultats
import json,sys,os,inspect
import time
from tempfile import NamedTemporaryFile
import subprocess

class ExercicePython:
    def charger_module(self,nom_module):
        try:
            self.module_ens=test_fonction.ouvre_module(nom_module)
            return True
        except test_fonction.Erreur as e:
            self.messagesErreur.append( str(e) )
        return False

    def parser_module(self):
        solution = [ (nom,fun) for nom,fun in self.module_ens.__dict__.items() if "solution" in dir(fun) ]

        if solution:
            (self.nom_solution,self.solution)=solution[0]
        else:
            print("ERREUR : le decorateur solution ne doit pas etre defini")
        self.arguments=inspect.getargspec(self.solution).args
        self.entrees_visibles=self.module_ens.__dict__.get("entrees_visibles",[])
        self.entrees_invisibles=self.module_ens.__dict__.get("entrees_invisibles",[])
        if all(not isinstance(i,tuple) for i in self.entrees_visibles):
            self.entrees_visibles=[(x,) for x in self.entrees_visibles]
        if all(not isinstance(i,tuple) for i in self.entrees_invisibles):
            self.entrees_invisibles=[(x,) for x in self.entrees_invisibles]

        if(solution and (self.entrees_visibles or self.entrees_invisibles)):
            self.messages.append( "Solutions et entrées, tout y est !" )
        else:
            if(self.entrees_visibles or self.entrees_invisibles):
                self.messagesErreur.append( "Il y a des entrées mais pas de quoi tester" )
                return False
            elif solution:
                self.messagesErreur.append( "Il faut des entrées")
                return False
            else:
                self.messagesErreur.append( "Il faut des entrées et de quoi tester" )
                return False
        if(self.entrees_visibles and not self.entrees_invisibles):
            self.messagesInfo.append( "Toutes les entrées sont visibles!" )
        if(self.entrees_invisibles and not self.entrees_visibles):
            self.messagesInfo.append("Toutes les entrées sont invisibles!")
        return True


    def tester_solution_ens(self):
        try:
            time0=time.time()
            self.solutions_visibles=test_fonction.test_fonction(self.solution,self.entrees_visibles)
            self.solutions_invisibles=test_fonction.test_fonction(self.solution,self.entrees_invisibles)
            temps=time.time()-time0
            self.temps=temps
            return True
        except test_fonction.Erreur as e:
            self.error=True
            self.messagesErreur.append( str(e) )
            return False
    def afficher(self):
        attrs=["messages","messagesErreur","messagesInfo","entrees_visibles","entrees_invisibles","solutions_visibles","solutions_invisibles","temps","nom_solution","arguments"]
        res={attr:self.__dict__[attr] for attr in attrs if self.__dict__[attr] }
        res["solutions_visibles"]=[(str(list(x))[1:-1],str(y)) for (x,y) in self.solutions_visibles]
        print(json.dumps(res))

    def __init__(self,module):
        self.messages=[]
        self.messagesErreur=[]
        self.messagesInfo=[]
        self.module_ens=None
        self.entrees_visibles=[]
        self.entrees_invisibles=[]
        self.solutions_visibles=[]
        self.solutions_invisibles=[]
        self.temps=None
        self.solution=None
        self.nom_solution=None
        self.arguments=None
        self.module_charge=False
        if (self.charger_module(module) and self.parser_module()):
            self.module_charge=True

    def tester_solution_etu(self,nom_module_etu):
        resultat=resultats()
        if not self.module_charge:
            resultat.invalide({"erreur":"probleme dans l'exercice"})
            return resultat
        try:
            module_etu=test_fonction.ouvre_module(nom_module_etu)
        except test_fonction.Erreur as e:
            resultat.invalide({"exception":e.exception, "erreur":e.erreur})
            return resultat

        if not self.nom_solution in dir(module_etu):
            resultat.invalide({"erreur":"Votre programme doit contenir une fonction "+self.nom_solution})
            return resultat
        try:
            time0=time.time()
            fonction_etudiant=getattr(module_etu,self.nom_solution)
            soletu_vi=test_fonction.test_fonction(fonction_etudiant,self.entrees_visibles)
            soletu_invi=test_fonction.test_fonction(fonction_etudiant,self.entrees_invisibles)
            temps=time.time()-time0
            resultat.temps(temps)
        except test_fonction.Erreur as e:
            resultat.invalide({"exception":e.exception, "erreur":e.erreur})
            return resultat
        zip_visibles=zip(self.solutions_visibles,soletu_vi)
        zip_invisibles=zip(self.solutions_invisibles,soletu_invi)
        mauvaises_sorties_visibles=[((str(ent_ens),str(sor_etu),str(sor_ens))) for ((ent_ens,sor_ens),(ent_etu,sor_etu)) in zip_visibles if sor_ens!=sor_etu]
        resultat.add_mauvaises_sorties("visibles", mauvaises_sorties_visibles)
        mauvaises_sorties_invisibles=[((" cachée"," quelque chose",("autre chose",""))) for ((ent_ens,sor_ens),(ent_etu,sor_etu)) in zip_invisibles if sor_ens!=sor_etu]
        resultat.add_mauvaises_sorties("invisibles",mauvaises_sorties_invisibles)
        return resultat



def ChargerExoEns(contenu):
    module_ens=NamedTemporaryFile(mode="wt",suffix=".py",encoding="utf-8", dir=os.path.dirname(__file__))
    debut="""def solution(fun):
    fun.solution=True
    return fun

"""
    module_ens.write(debut+contenu)
    module_ens.seek(0)
    temps_execution_disponible=1
    sortie_err=NamedTemporaryFile(mode="w+t",encoding="utf-8")
    sortie_std=NamedTemporaryFile(mode="w+t",encoding="utf-8")

    script="cd "+os.path.dirname(__file__)+"; python3 -E script.py chargeEns "+os.path.basename(module_ens.name).split(".")[0]
    #print(module_ens.name)
    erreur=subprocess.call("ulimit -v 555000; ulimit -t "+str(temps_execution_disponible)+";"+script,stdout=sortie_std,stderr=sortie_err,shell=True)
    sortie_std.seek(0)
    sortie_err.seek(0)
    sortie_erreur=sortie_err.read()
    sortie_standard=sortie_std.read()
    if(erreur!=0):
        return {"erreur":str(sortie_erreur)}
    else:
        return json.loads(sortie_standard)
    #exo=ExercicePython(os.path.basename(module_ens.name).split(".")[0])
    #print("TOTO")
    #exo.afficher()

    #return exo.messages


def TesterExoEns(contenu):
    module_ens=NamedTemporaryFile(mode="wt",suffix=".py",encoding="utf-8", dir=os.path.dirname(__file__))
    debut="""def solution(fun):
    fun.solution=True
    return fun

"""
    module_ens.write(debut+contenu)
    module_ens.seek(0)

    #import time
    #time.sleep(40)
    temps_execution_disponible=1
    sortie_err=NamedTemporaryFile(mode="w+t",encoding="utf-8")
    sortie_std=NamedTemporaryFile(mode="w+t",encoding="utf-8")

    script="cd "+os.path.dirname(__file__)+"; python3 -E script.py testEns "+os.path.basename(module_ens.name).split(".")[0]
    print(module_ens.name)
#    erreur=subprocess.call("ulimit -v 555000; ulimit -t "+str(temps_execution_disponible)+"; python3.3 -E "+ mkpath("../exercice_prog/Test_module/test_module_etu.py")+ " "+ os.path.basename(module_ens.name).split(".")[0] + " " + os.path.basename(module_etu.name).split(".")[0],stdout=sortie_std,stderr=sortie_err,shell=True)
    erreur=subprocess.call("ulimit -v 555000; ulimit -t "+str(temps_execution_disponible)+";"+script,stdout=sortie_std,stderr=sortie_err,shell=True)
    sortie_std.seek(0)
    sortie_err.seek(0)
    sortie_erreur=sortie_err.read()
    sortie_standard=sortie_std.read()
    if(erreur!=0):
        return {"erreur":str(sortie_erreur)}
    else:
        return json.loads(sortie_standard)
    #exo=ExercicePython(os.path.basename(module_ens.name).split(".")[0])
    #print("TOTO")
    #exo.afficher()

    #return exo.messages

def TesterSolutionEtu(contenuEns,contenuEtu):
    module_ens=NamedTemporaryFile(mode="wt",suffix=".py",encoding="utf-8", dir=os.path.dirname(__file__))
    debut="""def solution(fun):
    fun.solution=True
    return fun

"""
    module_ens.write(debut+contenuEns)
    module_ens.seek(0)

    module_etu=NamedTemporaryFile(mode="wt",suffix=".py",encoding="utf-8", dir=os.path.dirname(__file__))
    module_etu.write(contenuEtu)
    module_etu.seek(0)
    #import time
    #time.sleep(40)
    temps_execution_disponible=1
    sortie_err=NamedTemporaryFile(mode="w+t",encoding="utf-8")
    sortie_std=NamedTemporaryFile(mode="w+t",encoding="utf-8")

    script="cd "+os.path.dirname(__file__)+"; python3 -E script.py etu "+os.path.basename(module_ens.name).split(".")[0]+" " + os.path.basename(module_etu.name).split(".")[0]
#    erreur=subprocess.call("ulimit -v 555000; ulimit -t "+str(temps_execution_disponible)+"; python3.3 -E "+ mkpath("../exercice_prog/Test_module/test_module_etu.py")+ " "+ os.path.basename(module_ens.name).split(".")[0] + " " + os.path.basename(module_etu.name).split(".")[0],stdout=sortie_std,stderr=sortie_err,shell=True)
    erreur=subprocess.call("ulimit -v 555000; ulimit -t "+str(temps_execution_disponible)+";"+script,stdout=sortie_std,stderr=sortie_err,shell=True)
    sortie_std.seek(0)
    sortie_err.seek(0)
    sortie_erreur=sortie_err.read()
    sortie_standard=sortie_std.read()

## Scoring
    sortie_err=NamedTemporaryFile(mode="w+t",encoding="utf-8")
    sortie_std=NamedTemporaryFile(mode="w+t",encoding="utf-8")
    script="cd "+os.path.dirname(__file__)+"; pylint3 --disable=C0111 " +\
           os.path.basename(module_etu.name) + " | grep -A 2 evaluation | tail -1 | cut -d \" \" -f7 | cut -d\"/\" -f1"
    erreur2=subprocess.call(script, stdout=sortie_std,stderr=sortie_err,shell=True)
    sortie_std.seek(0)
    sortie_err.seek(0)
    score=sortie_std.read()
    if(erreur!=0):
        return {"erreur":str(sortie_erreur)}
    else:
        dico=json.loads(sortie_standard)
        dico["score"]=score
        return dico
    #exo=ExercicePython(os.path.basename(module_ens.name).split(".")[0])
    #print("TOTO")
    #exo.afficher()

    #return exo.messages
