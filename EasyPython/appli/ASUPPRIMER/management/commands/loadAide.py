from django.core.management.base import BaseCommand
import yaml
from django.db import models, migrations

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd pour les aides a partir d un fichier yaml'
	
	def handle(self, *args, **options):
		users=yaml.load(open('./appli/Yaml/aide.yml'))
	
		#import des modeles.
		from appli.models import Aide
	
		# On crée les utilisateurs.
		i=0
		for b in users:
			new_aide=Aide(cle=b['cle'],
				definition=b['definition'])
				
			new_aide.save()
			
    
