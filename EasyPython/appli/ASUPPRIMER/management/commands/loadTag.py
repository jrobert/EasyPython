from django.core.management.base import BaseCommand
import yaml
from django.db import models, migrations

class Command(BaseCommand, migrations.Migration):
	args = '<team_id>'
	help = 'Met a jour la bd pour les tags a partir d un fichier yaml'
	
	def handle(self, *args, **options):
		tag=yaml.load(open('./appli/Yaml/tag.yml'))
	
		#import des modeles.
		from appli.models import Tag
	
		# On crée les vins.
		i=0
		for b in tag:
			new_tag=Tag(nom=b['nom'])
			new_tag.save()
			
    
