#!/usr/bin/python3
from Exercice import *
exo=ExercicePython(sys.argv[1])
exo.tester_solution_ens()
print("EXO")

print("Nom que doit porter la solution : ", exo.nom_solution)
print("Avec les arguments : ", exo.arguments)
print(exo.nom_solution+"(",end="")
for arg in exo.arguments[:-1]:
    print(arg,end=",")
print(exo.arguments[-1],end=")\n")

exo.afficher()
resultats=exo.tester_solution_etu(sys.argv[2])
print("RESULTATS")
print(resultats.dumps())
#print(json.dumps(x.tester_solution_etu(sys.argv[2])))

